<?php

return [

    'system_locale' => 'en',

    'system_prefix' => 'system',

    'system_route_prefix' => 'earth.system',

    'system_domain' => false,

    'system_name' => 'Earth',

    'system_login_key' => 'email',

    'system_after_login_redirect' => 'system',

    'website_locale' => '',

    'website_news_text_types' => ['news'],

    'website_form' => [
        'sender_email' => env('SENDER_EMAIL', 'lovro.papez@gmail.com'),
        'sender_name' => env('SENDER_NAME', 'Lovro Papež'),
        'destination_email' => env('DESTINATION_EMAIL', 'lovro.papez@gmail.com'),
        'email_subject' => env('EMAIL_SUBJECT', 'New contact form submission'),
    ],

    /**
     * Texts "image" sizes
     */
    'texts_image_sizes' => [

        // Image
        'image' => [
            'styles' => [
                'large' => ['dimensions' => '900', 'auto_orient' => true],
                'medium' => ['dimensions' => '300', 'auto_orient' => true],
                'thumb' => ['dimensions' => '120x120#', 'auto_orient' => true]
            ],
            'url' => '/media/:attachment/:id_partition/:style/:filename'
        ]

    ],

    /**
     * Gallery "image" sizes
     */
    'gallery_image_sizes' => [

        // File
        'file' => [
            'styles' => [
                'large' => ['dimensions' => '900', 'auto_orient' => true],
                'medium' => ['dimensions' => '300', 'auto_orient' => true],
                'thumb' => ['dimensions' => '120x120#', 'auto_orient' => true]
            ],
            'url' => '/media/:attachment/:id_partition/:style/:filename'
        ]

    ],

    /**
     * File attachments
     */
    'file_attachments' => [
        'file' => [
            'url' => '/media/:attachment/:id_partition/:style/:filename'
        ]
    ],

    /**
     * Mime types
     */
    'mime_types' => [
        'application/zip' => 'archive',
        'application/pdf' => 'file-pdf-o',
        'application/msword' => 'file-word-o',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'file-word-o',
        'application/vnd.ms-excel' => 'file-excel-o',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'file-excel-o',
        'application/vnd.ms-powerpoint' => 'file-powerpoint-o',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'file-powerpoint-o',
        'application/x-shockwave-flash' => 'file-video-o',
    ]

];