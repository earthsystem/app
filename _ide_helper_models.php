<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property string $last_login
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $type
 * @property string $username
 * @property integer $can_modify
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCanModify($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDeletedAt($value)
 */
	class User extends \Eloquent {}
}

namespace Earth\Core\Entities\Activity{
/**
 * Earth\Core\Entities\Activity\Activity
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $ip_address
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Earth\Core\Entities\User\User $user
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereIpAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Activity\Activity whereUpdatedAt($value)
 */
	class Activity extends \Eloquent {}
}

namespace Earth\Core\Entities\Attachment{
/**
 * Earth\Core\Entities\Attachment\Attachment
 *
 * @property integer $id
 * @property integer $media_id
 * @property integer $attachable_id
 * @property string $attachable_type
 * @property string $type
 * @property integer $position
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $attachable
 * @property-read \Earth\Core\Entities\Attachment\Media $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Attachment\Locale[] $locales
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereMediaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereAttachableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereAttachableType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Attachment whereUpdatedAt($value)
 */
	class Attachment extends \Eloquent {}
}

namespace Earth\Core\Entities\Attachment{
/**
 * Earth\Core\Entities\Attachment\File
 *
 * @property integer $id
 * @property string $file_file_name
 * @property integer $file_file_size
 * @property string $file_content_type
 * @property string $file_updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereFileFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereFileFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereFileContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereFileUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\File whereUpdatedAt($value)
 */
	class File extends \Eloquent {}
}

namespace Earth\Core\Entities\Attachment{
/**
 * Earth\Core\Entities\Attachment\Image
 *
 * @property integer $id
 * @property string $file_file_name
 * @property integer $file_file_size
 * @property string $file_content_type
 * @property string $file_updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereFileFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereFileFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereFileContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereFileUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Image whereUpdatedAt($value)
 */
	class Image extends \Eloquent {}
}

namespace Earth\Core\Entities\Attachment{
/**
 * Earth\Core\Entities\Attachment\Locale
 *
 * @property integer $id
 * @property integer $attachment_id
 * @property string $title
 * @property string $comment
 * @property string $locale
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereAttachmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Locale whereUpdatedAt($value)
 */
	class Locale extends \Eloquent {}
}

namespace Earth\Core\Entities\Attachment{
/**
 * Earth\Core\Entities\Attachment\Media
 *
 * @property integer $id
 * @property string $file_file_name
 * @property integer $file_file_size
 * @property string $file_content_type
 * @property string $file_updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereFileFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereFileFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereFileContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereFileUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Attachment\Media whereUpdatedAt($value)
 */
	class Media extends \Eloquent {}
}

namespace Earth\Core\Entities\Data{
/**
 * Earth\Core\Entities\Data\Data
 *
 * @property integer $id
 * @property integer $customable_id
 * @property string $customable_type
 * @property string $customable_key
 * @property string $customable_value
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $customable
 * @property-read mixed $key
 * @property-read mixed $value
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCustomableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCustomableType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCustomableKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCustomableValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Data\Data whereUpdatedAt($value)
 */
	class Data extends \Eloquent {}
}

namespace Earth\Core\Entities\Element{
/**
 * Earth\Core\Entities\Element\Element
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $active
 * @property string $name
 * @property string $data
 * @property string $icon
 * @property string $type
 * @property integer $can_modify
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $parent_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Element\Locale[] $locales
 * @property-read \Earth\Core\Entities\Element\Locale $locale
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Data\Data[] $custom_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Template\Template[] $templates
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Text\Text[] $texts
 * @property-read \Earth\Core\Entities\Element\Element $parent
 * @property-read \Baum\Extensions\Eloquent\Collection|\Earth\Core\Entities\Element\Element[] $children
 * @property-read mixed $created_by_name
 * @property-read mixed $updated_by_name
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereRgt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereIcon($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereCanModify($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element hasSlug($slug, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element hasLocale($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element withLocale($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutSelf()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node limitDepth($limit)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Element search($q = null, $fields = array())
 */
	class Element extends \Eloquent {}
}

namespace Earth\Core\Entities\Element{
/**
 * Earth\Core\Entities\Element\Locale
 *
 * @property integer $id
 * @property integer $element_id
 * @property string $locale
 * @property string $title
 * @property string $slug
 * @property string $comment
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Locale whereUpdatedAt($value)
 */
	class Locale extends \Eloquent {}
}

namespace Earth\Core\Entities\Element{
/**
 * Earth\Core\Entities\Element\Template
 *
 * @property integer $id
 * @property integer $active
 * @property integer $element_id
 * @property integer $template_id
 * @property integer $recursive
 * @property integer $position
 * @property string $type
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereTemplateId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereRecursive($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Template whereUpdatedAt($value)
 */
	class Template extends \Eloquent {}
}

namespace Earth\Core\Entities\Element{
/**
 * Earth\Core\Entities\Element\Text
 *
 * @property integer $id
 * @property integer $element_id
 * @property integer $text_id
 * @property integer $position
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereElementId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereTextId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Element\Text whereUpdatedAt($value)
 */
	class Text extends \Eloquent {}
}

namespace Earth\Core\Entities{
/**
 * Earth\Core\Entities\Node
 *
 * @property-read \Earth\Core\Entities\Node $parent
 * @property-read \Baum\Extensions\Eloquent\Collection|\Earth\Core\Entities\Node[] $children
 * @property-read mixed $created_by_name
 * @property-read mixed $updated_by_name
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutSelf()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node limitDepth($limit)
 */
	class Node extends \Eloquent {}
}

namespace Earth\Core\Entities\Permission{
/**
 * Earth\Core\Entities\Permission\Permission
 *
 * @property integer $id
 * @property integer $object_id
 * @property string $object_type
 * @property string $type
 * @property integer $access_id
 * @property string $access_type
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereObjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereObjectType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereAccessId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereAccessType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Permission\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace Earth\Core\Entities\Role{
/**
 * Earth\Core\Entities\Role\Role
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $permissions
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $can_modify
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\User\User[] $users
 * @property-read mixed $created_by_name
 * @property-read mixed $updated_by_name
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereCanModify($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role doesNotHaveUser($id)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role sort($value = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role doesNotHavePermission($id, $field, $objectType)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role hasPermission($id, $field, $objectType)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Role\Role search($q = null, $fields = array())
 */
	class Role extends \Eloquent {}
}

namespace Earth\Core\Entities\Template{
/**
 * Earth\Core\Entities\Template\Template
 *
 * @property integer $id
 * @property string $path
 * @property string $name
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Baum\Extensions\Eloquent\Collection|\Earth\Core\Entities\Element\Element[] $elements
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template notInElementAndPosition($id, $position)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Template\Template search($q = null, $fields = array())
 */
	class Template extends \Eloquent {}
}

namespace Earth\Core\Entities\Text{
/**
 * Earth\Core\Entities\Text\Locale
 *
 * @property integer $id
 * @property integer $text_id
 * @property integer $active
 * @property string $title
 * @property string $slug
 * @property string $subtitle
 * @property string $summary
 * @property string $data
 * @property string $locale
 * @property string $tags
 * @property \Carbon\Carbon $publish_from
 * @property \Carbon\Carbon $publish_to
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Attachment\Attachment[] $attachments
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereTextId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereSubtitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereSummary($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereTags($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale wherePublishFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale wherePublishTo($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Locale whereUpdatedAt($value)
 */
	class Locale extends \Eloquent {}
}

namespace Earth\Core\Entities\Text{
/**
 * Earth\Core\Entities\Text\Text
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $image_file_name
 * @property integer $image_file_size
 * @property string $image_content_type
 * @property string $image_updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Text\Locale[] $locales
 * @property-read \Earth\Core\Entities\Text\Locale $locale
 * @property-read \Baum\Extensions\Eloquent\Collection|\Earth\Core\Entities\Element\Element[] $elements
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Data\Data[] $custom_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Attachment\Attachment[] $attachments
 * @property-read mixed $created_by_name
 * @property-read mixed $updated_by_name
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereImageFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereImageFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereImageContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereImageUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text notInElement($id)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text sort($value = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text hasLocale($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text withLocale($locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text hasSlug($slug, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text isPublished($date = null, $locale = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\Text\Text search($q = null, $fields = array())
 */
	class Text extends \Eloquent {}
}

namespace Earth\Core\Entities\User{
/**
 * Earth\Core\Entities\User\User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property string $last_login
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $type
 * @property string $username
 * @property integer $can_modify
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Earth\Core\Entities\Role\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cartalyst\Sentinel\Persistences\EloquentPersistence[] $persistences
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cartalyst\Sentinel\Activations\EloquentActivation[] $activations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cartalyst\Sentinel\Reminders\EloquentReminder[] $reminders
 * @property-read \Illuminate\Database\Eloquent\Collection|\Cartalyst\Sentinel\Throttling\EloquentThrottle[] $throttle
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereCanModify($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User doesNotHaveRole($id)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User sort($value = null)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User doesNotHavePermission($id, $field, $objectType)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User hasPermission($id, $field, $objectType)
 * @method static \Illuminate\Database\Query\Builder|\Earth\Core\Entities\User\User search($q = null, $fields = array())
 */
	class User extends \Eloquent {}
}

