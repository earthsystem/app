$(document).ready(function(){
    
    $(".fancybox").fancybox({
        padding: 0,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: { thumbs: true }
    });

    if ( $("#nav").size() ) {
        $("#nav").menuzord({
            scrollable: true,
            indicatorFirstLevel: "+"
        });
    }

    $("iframe").each(function(){
        $(this).wrap("<div class='embed-container'></div>");
    });

});