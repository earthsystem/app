<?php

view()->composer('earth.page.index', Earth\Core\Composers\Web\PageComposer::class);
view()->composer('earth.news.archive', Earth\Core\Composers\Web\NewsSidebarComposer::class);
view()->composer('earth.news.list', Earth\Core\Composers\Web\NewsListComposer::class);