@if ($files->count())
    <hr>
    <h4>{{earthLang('web.files')}}</h4>

    <ul class="list-group">
        @foreach($files as $file)
            <li class="list-group-item">
                <a href="{{$file->media->file->url()}}" target="_blank">
                    {!! file_type_icon( $file->media->file->contentType() ) !!}
                    {{$file->locale->title}}
                    ({{file_size( $file->media->file->size() )}})
                </a>
            </li>
        @endforeach
    </ul>
@endif