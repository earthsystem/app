@if ($gallery->count())
    <hr>
    <h4>{{earthLang('web.gallery')}}</h4>

    @foreach( array_chunk($gallery->all(), 4) as $photoGroup )
        <div class="row">
            @foreach($photoGroup as $photo)
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <a href="{{$photo->media->file->url('large')}}"
                       class="fancybox gallery-img"
                       @if ( isset($photo->locale->title) ) title="{{$photo->locale->title}}" @endif
                       rel="{{$text->id}}">
                        <img src="{{$photo->media->file->url('thumb')}}" alt="">
                    </a>
                </div>
            @endforeach
        </div>
    @endforeach
@endif