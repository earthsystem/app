<div class="alert alert-info alert-fixed" role="alert" style="position: fixed; bottom: 0; width: 100%; margin-bottom: 0px;">
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
            <i class="fa fa-info-circle fa-2x"></i>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-7 col-xs-7">
            {{trans('cookies.text')}}
            <a href="/{{$locale}}/cookie-settings" class="link">{{trans('cookies.more')}}</a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
            <a href="#" class="btn btn-default btn-xs btn-block" data-dismiss="alert">{{trans('cookies.ok')}}</a>
        </div>
    </div>
</div>