<title>
    @if ( has_article() ) {{$article->title}} &raquo; @endif
    @if ( has_category() and !is_home() ) {{$category->title}} &raquo; @endif
    {{$application->name}}
</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="token" content="{{csrf_token()}}">
<meta name="description" content="">
<meta http-equiv="content-language" content="{{$locale}}">
<meta name="generator" content="">
<meta name="robots" content="index,follow">
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

@if ( has_article() )
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{$article->title}}" />
    @if ($article->hasImage())
        <meta property="og:image" content="{{asset( $article->image->url('large') )}}" />
    @endif
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="{{url()->current()}}">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="{{$article->title}}">
    <meta name="twitter:description" content="{{$article->getParsedSummary()}}">
    @if ($article->hasImage())
        <meta property="twitter:image" content="{{asset( $article->image->url('large') )}}" />
    @endif
@endif

<!-- css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

<link rel="stylesheet" href="{{earthAsset('vendor/fancybox-v3/jquery.fancybox.css')}}">
<link rel="stylesheet" href="{{earthAsset('vendor/fancybox-v3/jquery.fancybox-thumbs.css')}}">

<link rel="stylesheet" href="{{earthAsset('vendor/menuzord/menuzord.css')}}">

@if ( file_exists( public_path("assets/css/main.css") ) )
    <link rel="stylesheet" href="{{asset("assets/css/main.css")}}">
@endif

@yield('css')
<!-- // css -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->