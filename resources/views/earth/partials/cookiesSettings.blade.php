<div class="alert alert-warning">
    <div class="row">
        <div class="col-lg-12">
            <h4>{{trans('cookies.change')}}</h4>

            <p>
                @if (!isset($_COOKIE['cookiesAreBlocked']))
                    <a href="/{{$locale}}/block-cookies/1">{{trans('cookies.block')}}</a>
                @else
                    <a href="/{{$locale}}/block-cookies/0">{{trans('cookies.unblock')}}</a>
                @endif
            </p>
        </div>
    </div>
</div>