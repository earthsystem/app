<!-- javascript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="{{earthAsset('vendor/fancybox-v3/jquery.fancybox.js')}}"></script>
<script src="{{earthAsset('vendor/fancybox-v3/jquery.fancybox-thumbs.js')}}"></script>

<script src="{{earthAsset('vendor/menuzord/menuzord.js')}}"></script>

@if ( file_exists( public_path("assets/js/main.js") ) )
    <script src="{{asset("assets/js/main.js")}}"></script>
@endif

@if (!isset($_COOKIE['cookiesAreBlocked']) and env('ANALYTICS_CODE') !== null)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '{{env('ANALYTICS_CODE')}}', 'auto');
        ga('send', 'pageview');

    </script>
@endif

@yield('javascript')
<!-- // javascript -->