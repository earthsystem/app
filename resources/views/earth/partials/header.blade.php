<div class="container">

    <div id="nav" class="menuzord red">

        <a href="/{{$locale}}" class="menuzord-brand hidden-lg hidden-md">
            {{$application->name}}
        </a>

        <ul class="menuzord-menu">
            <li class="hidden-sm hidden-xs">
                <a href="/{{$locale}}" class="menuzord-brand">
                    {{$application->name}}
                </a>
            </li>

            @foreach(web_navigation() as $item)
                <li>
                    <a href="{{ $item->isUrl() ? $item->getUrl() : $item->getInternalUrl() }}"  @if($item->isUrl()) target="_blank" @endif class="{{$item->isActive($item->depth - 3)}}">
                        {{$item->title}}
                    </a>

                    @if ( $item->submenu->count() )
                        <ul class="dropdown">
                            @foreach($item->submenu as $subItem)
                                <li>
                                    <a href="{{ $subItem->isUrl() ? $subItem->getUrl() : $subItem->getInternalUrl([$item->slug]) }}"  @if($subItem->isUrl()) target="_blank" @endif>
                                        {{$subItem->title}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>

    </div>

</div>