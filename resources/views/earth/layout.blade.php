<!doctype html>
<html>
    <head>
        {!! $head or '' !!}
    </head>
    <body>

        @if ( isset($header) ) {!! $header !!} @endif

        @if ( isset($templates) )

            @foreach($templates->top as $template)
                @if ( view()->exists($template->path) ) @include($template->path) @endif
            @endforeach

            @foreach($templates->main as $template)
                @if ( view()->exists($template->path) ) @include($template->path) @endif
            @endforeach

            @if ($templates->center->count())

                <div class="container">
                    <div class="row">

                        @if ($templates->left->count())
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                @foreach($templates->left as $template)
                                    @if ( view()->exists($template->path) ) @include($template->path) @endif
                                @endforeach
                            </div>
                        @endif

                        @if ($templates->center->count())
                            <div class="col-lg-{{column_width($templates)}} col-md-{{column_width($templates)}} col-sm-12 col-xs-12">
                                @foreach($templates->center as $template)
                                    @if ( view()->exists($template->path) ) @include($template->path) @endif
                                @endforeach
                            </div>
                        @endif

                        @if ($templates->right->count())
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                @foreach($templates->right as $template)
                                    @if ( view()->exists($template->path) ) @include($template->path) @endif
                                @endforeach
                            </div>
                        @endif

                    </div>
                </div>

            @endif

        @endif

        @if ( isset($footer) ) @include($footer) @endif

        <a href="/{{$locale}}/cookie-settings">
            {{trans('cookies.cookie-settings')}}
        </a>

        @if (!isset($_COOKIE['hasSeenCookieNotification']) and env('ANALYTICS_CODE') !== null)
            @include( $application->data . '.partials.cookiesNotification')
        @endif

        {!! $javascript or '' !!}
    </body>
</html>