<div class="container">
    <div class="jumbotron">
        <h1><i class="fa fa-exclamation-circle"></i> {{earthLang('web.error_title')}}</h1>
        <p>{{earthLang('web.error_desc')}}</p>
    </div>
</div>