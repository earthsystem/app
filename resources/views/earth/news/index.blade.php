@if ( has_article() )
    @include($application->data . '.news.detail')
@else
    @include($application->data . '.news.list')
@endif