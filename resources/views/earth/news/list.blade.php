<h5>
    {{earthLangWithVars('web.showing_page_of', ["current" => $texts->currentPage(), "total" => $texts->lastPage()])}}
</h5>
<hr>

{!! $texts->appends(['year' => isset($requestedYear) ? $requestedYear : null, 'month' => isset($requestedMonth) ? $requestedMonth : null ])->render() !!}

@foreach($texts as $text)

    <div class="row article">
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
            @if ( $text->hasImage() )
                <a href="{{$text->getUri(0)}}">
                    <img src="{{$text->image->url('thumb')}}" alt="" class="img-responsive">
                </a>
            @endif
        </div>
        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
            <h3 class="title">
                <a href="{{$text->getUri(0)}}">{{$text->title}}</a>
            </h3>
            <p class="text-muted">
                <i class="fa fa-calendar"></i>
                {{$text->created_at->format("d.m.Y")}}
            </p>
            <p>
                {!! $text->getParsedSummary() !!}
                <a href="{{$text->getUri(0)}}">{{earthLang('web.read_more')}}</a>
            </p>
            <p class="text-muted">
                {{earthLangWithVars('web.created_by', ["name" => $text->createdByName])}}
            </p>
        </div>
    </div>
    <hr>
@endforeach

{!! $texts->appends([
    'year' => isset($requestedYear) ? $requestedYear : null,
    'month' => isset($requestedMonth) ? $requestedMonth : null,
    'q' => isset($q) ? $q : null,
])->render() !!}