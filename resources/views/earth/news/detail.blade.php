<h3>{{$article->title}}</h3>

<h5 class="text-muted">
    <i class="fa fa-calendar"></i>
    {{$article->created_at->format("d.m.Y")}}
    |
    {{$article->createdByName}}
</h5>

@if ( !empty($article->subtitle) ) <h4>{{$article->subtitle}}</h4> @endif
@if ( !empty($article->summary) ) <p class="summary">{{$article->summary}}</p> @endif

@if ( $article->hasImage() )
    <div class="pull-left">
        <a href="{{$article->image->url('large')}}" class="fancybox" rel="{{$article->id}}">
            <img src="{{$article->image->url('thumb')}}" alt="{{$article->title}}" class="img-thumbnail marginalize">
        </a>
    </div>
@endif

<p>{!! $article->parsedContent !!}</p>
<div class="clearfix"></div>

@include($application->data . ".widgets.gallery", ["gallery" => $article->gallery()->with('locale')->get(), "text" => $article])
@include($application->data . ".widgets.files", ["files" => $article->files()->withLocale()->get(), "text" => $article])