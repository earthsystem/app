<h4>{{earthLang('web.archive')}}</h4>

{!! Form::open(['method' => 'get', 'url' => '/'.app()->getLocale().'/'.slug(0), 'class' => 'mb-20']) !!}
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="q" id="q" value="{{request()->get('q')}}">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
            </button>
        </div>
    </div>
{!! Form::close() !!}

@foreach($years as $year)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a href="/{{app()->getLocale()}}/{{slug(0)}}/{{$year->year}}">
                    {{$year->year}}
                </a>
            </h3>
        </div>

        @if ( ( isset($requestedYear) and $year->year == $requestedYear)
                or ( !isset($requestedYear) and $year->year == date("Y") ) )
            <ul class="list-group submenu">
                @foreach($year->months as $month)
                    <li class="list-group-item @if (isset($requestedMonth) and $month->month == $requestedMonth) active @endif">
                        <a href="/{{app()->getLocale()}}/{{slug(0)}}/{{$year->year}}/{{$month->month}}">
                            {{earthLang('web.months.'.$month->month)}}
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
@endforeach