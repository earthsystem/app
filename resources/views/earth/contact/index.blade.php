<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

            <h1>{{$category->title}}</h1>

            {!! Form::open(['url' => route('earth.sendForm', app()->getLocale() ), 'method' => 'post']) !!}

                @if (session()->has('error'))
                    <div class="alert alert-danger">
                        {{env('APP_DEBUG') ? session()->get('error') : earthLang('web.contact_form_not_sent')}}
                    </div>
                @endif

                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{earthLang('web.contact_form_sent')}}
                    </div>
                @endif

                <div class="form-group">
                    <label for="name">{{earthLang('web.name')}}</label>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="email">{{earthLang('web.email')}}</label>
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="message">{{earthLang('web.message')}}</label>
                    {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 4]) !!}
                </div>

                {!! app('captcha')->display() !!}
            
                <button class="btn btn-primary mt-20">{{earthLang('web.submit')}}</button>

            {!! Form::close() !!}

        </div>
    </div>
</div>