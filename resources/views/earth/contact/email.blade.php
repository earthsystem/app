<!doctype html>
<html>
<head>
    <title>{{config('earth.website_form.email_subject')}}</title>
    <meta charset="utf-8"/>
</head>
<body>

<p>{!! earthLang('web.new_message_from_the_website') !!}</p>

@foreach($data as $key => $value)
    <strong>{{earthLang('web.'.$key)}}:</strong> {{$value}}<br/>
@endforeach
<br/>
{{earthLang('web.have_a_good_day')}}
</body>
</html>