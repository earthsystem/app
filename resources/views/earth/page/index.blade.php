@foreach($texts as $text)

    <h3>{{$text->title}}</h3>

    @if ( !empty($text->subtitle) ) <h4>{{$text->subtitle}}</h4> @endif
    @if ( !empty($text->summary) ) <p class="summary">{{$text->summary}}</p> @endif
    
    @if ( $text->hasImage() )   
        <div class="pull-left">
            <a href="{{$text->image->url('large')}}" class="fancybox" rel="{{$text->id}}">
                <img src="{{$text->image->url('thumb')}}" alt="{{$text->title}}" class="img-thumbnail marginalize">
            </a>
        </div>
    @endif

    <p>{!! $text->parsedContent !!}</p>
    <div class="clearfix"></div>

    @include($application->data . ".widgets.gallery", ["gallery" => $text->gallery()->with('locale')->get(), "text" => $text])
    @include($application->data . ".widgets.files", ["files" => $text->files()->withLocale()->get(), "text" => $text])

@endforeach