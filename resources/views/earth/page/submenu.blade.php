<ul class="list-group submenu">
    {!!
        submenu(
            0, // Category index of the slug - or collection of items
            4, // Level to stop at, this means to stop at slug(4)
            app()->getLocale().'/'.slug(0) // Base of the slug
        )
    !!}
</ul>