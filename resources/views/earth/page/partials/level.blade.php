@if ( $items->count() )

    @foreach($items as $item)

        <li class="list-group-item level-{{$currentLevel}} @if( slug($currentLevel + 1) == $item->slug and $currentLevel == count(slugs()) - 2 ) active @endif">
            <a href="{{ $item->isUrl() ? $item->getUrl() : $item->getInternalUrl() }}"  @if($item->isUrl()) target="_blank" @endif class="{{$item->isActive($item->depth - 3)}}">
                {{$item->title}}
            </a>
            @if ( $item->submenu->count() and slug($currentLevel + 1) == $item->slug )
                {!!
                    submenu(
                        $item->submenu,
                        $stopAtLevel,
                        $currentSlug."/".$item->slug,
                        $view,
                        $currentLevel+1
                    )
                 !!}
            @endif
        </li>
    @endforeach

@endif