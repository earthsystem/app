<?php

return [
    'ok' => 'OK',
    'more' => 'more...',
    'text' => 'This website uses cookies for improved functionality. By continuing to visit this page, you agree to their use.',
    'change' => 'Change cookie settings',
    'block' => 'No, I do not want this website to use cookies for my better user experience.',
    'unblock' => 'Yes, I want this website to use cookies for my better user experience.',
    'cookie-settings' => 'Cookie settings',
];